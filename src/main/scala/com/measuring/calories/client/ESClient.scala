package com.measuring.calories.client

import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient

trait ESClient {
  implicit val client: RestClient = RestClient.builder(new HttpHost("localhost", 9200)).build()
}
