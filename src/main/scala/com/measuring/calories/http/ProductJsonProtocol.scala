package com.measuring.calories.http

import com.measuring.calories.model.Product
import spray.json.DefaultJsonProtocol

trait ProductJsonProtocol extends DefaultJsonProtocol{

  implicit val productFormat = jsonFormat7(Product)

}
