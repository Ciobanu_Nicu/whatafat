package com.measuring.calories.http

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.stream.ActorMaterializer
import akka.util.Timeout
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask

import com.measuring.calories.model.ActorModel.{AddProduct, FindAll, FindByField}
import com.measuring.calories.model.Product
import com.measuring.calories.service.ActorService

import scala.concurrent.Future
import scala.concurrent.duration._

trait ProductRest extends ProductJsonProtocol with SprayJsonSupport {

  implicit val system = ActorSystem("ProductRoutes")
  implicit val materializer = ActorMaterializer()
  import system.dispatcher

  implicit val defaultTimeout = Timeout(2 seconds)
  val productService = system.actorOf(Props[ActorService], "ProductService")

  val routes =
    pathPrefix("api" / "product") {
      get {
        parameters('field, 'value) { (fieldName, fieldValue) =>
          complete((productService ? FindByField(fieldName, fieldValue)).mapTo[Future[Seq[Product]]])
        } ~
        pathEndOrSingleSlash {
          complete((productService ? FindAll).mapTo[Future[Seq[Product]]])
        }
      } ~
      post {
        entity(as[Product]) { product =>
          complete((productService ? AddProduct(product)).map(_ => StatusCodes.OK))
        }
      }
    }
}
