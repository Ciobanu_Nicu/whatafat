package com.measuring.calories.model

object ActorModel {

  case class AddProduct(product: Product)
  case class ProductAdded(product: Product)
  case class FindByField(fieldName: String, fieldValue: String)
  case object FindAll

}
