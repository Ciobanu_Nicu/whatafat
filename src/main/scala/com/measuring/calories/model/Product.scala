package com.measuring.calories.model

case class Product(foodStuffID: String, foodStuffName: String, calories: Double, protein: Double, lipids: Double, carbohydrates: Double, fibers: Double)
