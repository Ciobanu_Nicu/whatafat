package com.measuring.calories.service

import akka.actor.{Actor, ActorLogging}

import com.measuring.calories.model.ActorModel._

class ActorService extends Actor with ActorLogging {

import com.measuring.calories.service.ESService._
  override def receive: Receive = {
    case FindAll =>
      log.info("Searching for the products")
      sender() ! readAll

    case FindByField(fieldName, fieldValue) =>
      log.info(s"Searching product by $fieldName with $fieldValue")
      sender() ! readByField(fieldName,fieldValue)

    case AddProduct(product) =>
      log.info(s"Add product $product")
      sender() ! write(product)
  }
}
