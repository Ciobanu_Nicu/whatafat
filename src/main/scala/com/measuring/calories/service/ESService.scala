package com.measuring.calories.service

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.elasticsearch.{ElasticsearchSourceSettings, WriteMessage}
import akka.stream.alpakka.elasticsearch.scaladsl.{ElasticsearchFlow, ElasticsearchSource}
import akka.stream.scaladsl.{Sink, Source}

import com.measuring.calories.client.ESClient
import com.measuring.calories.http.ProductJsonProtocol
import com.measuring.calories.model.Product

import scala.concurrent.Future

object ESService extends ESClient with ProductJsonProtocol{

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  def write(product: Product): Future[Seq[Unit]] = {

    val productToES = List[WriteMessage[Product, NotUsed]](
      WriteMessage.createUpsertMessage(id = product.foodStuffID, source = product)
    )

    Source(productToES)
      .via(
        ElasticsearchFlow.create[Product](
          "product",
          "foodstuff"
        )
      )
      .map {result =>
        if(!result.success)
          throw new Exception("Failed to write message to elastic")
      }
      .runWith(Sink.seq)
  }

  def readAll: Future[Seq[Product]] = {
    ElasticsearchSource
      .typed[Product](
      "product",
      Some("foodstuff"),
      searchParams = Map(
        "query" -> s""" {"match_all": {}} """
      ),
      ElasticsearchSourceSettings())
      .map { message =>
        message.source
      }
      .runWith(Sink.seq)
  }

  def readByField(fieldName: String, fieldValue: String): Future[Seq[Product]] = {
    ElasticsearchSource
      .typed[Product](
      "product",
      Some("foodstuff"),
      searchParams = Map(
        "query" -> s""" {"match": {"$fieldName":"$fieldValue"}} """
      ),
      ElasticsearchSourceSettings())
      .map { message =>
        message.source
      }
      .runWith(Sink.seq)
  }

}
