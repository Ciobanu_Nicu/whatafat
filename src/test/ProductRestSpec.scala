package test

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest

import com.measuring.calories.client.ESClient
import com.measuring.calories.http.ProductRest
import com.measuring.calories.model.Product

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.{ExecutionContext, Future}

class ProductRestSpec extends FlatSpec with ScalatestRouteTest with ESClient with ScalaFutures {

  trait TestProductRest extends ProductRest {
    def executionContext: ExecutionContext = executor
  }

  "RestRoutes" should "add the product" in new TestProductRest {
    val product = Product("0001", "chicken breast",114, 21.2, 2.6, 0, 0)

    Post("/api/product", product) ~> Route.seal(routes) ~> (check {
      assert(status == StatusCodes.OK)
    })
  }

  "RestRoutes" should "return the all products" in new TestProductRest {
    Get("/api/product") ~> Route.seal(routes) ~> (check {
      assert(status == StatusCodes.OK)
    })
  }

  "RestRoutes" should "return the product by field and value" in new TestProductRest {
    Get("/api/product?field=foodStuffName&value=chicken%20breast") ~> Route.seal(routes) ~> (check {
      assert(status == StatusCodes.OK)
    })
  }
}
